package exercise;

// BEGIN
public class NegativeRadiusException extends Exception {

	public NegativeRadiusException(String msg) {
		System.out.println(msg);
	}
}
// END
