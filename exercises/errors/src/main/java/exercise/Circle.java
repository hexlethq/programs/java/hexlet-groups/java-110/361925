package exercise;

// BEGIN
public class Circle {

	private final Point center;
	private final int radius;

	public Circle(final Point point, final int radius) {
		center = point;
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}

	public double getSquare() throws NegativeRadiusException {

		if (radius < 0) {
			throw new NegativeRadiusException("Не удалось посчитать площадь");
		}
		return Math.PI * Math.pow(radius, 2);
	}

}
// END
