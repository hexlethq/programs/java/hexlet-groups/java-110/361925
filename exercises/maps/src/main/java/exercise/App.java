package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

// BEGIN
class App {
    public static Map<String, Integer> getWordCount(String sentence) {
        Map<String, Integer> result = new HashMap<>();
        String[] arWords = sentence.split(" ");
        for (String s : arWords) {
            if (!s.isEmpty()) {
                if (!result.containsKey(s)) {
                    result.put(s, 1);
                } else {
                    result.put(s, result.get(s) + 1);
                }
            }
        }
        return result;

    }

    public static String toString(Map<String, Integer> map) {
        String result = "{}";
        if (!map.isEmpty()) {
            result = "{\n";
            for (String s : map.keySet()) {
                result += "  " + s + ": " + map.get(s) + "\n";
            }
            result += "}\n";
        }
        return result;
    }
}
//END
