package exercise.repository;

import exercise.model.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {

    // BEGIN
    public Iterable<Comment> findByPostId(long id);

    public Comment findByIdAndPostId(long commentId, long postId);

    // END
}
