package exercise.controller;

import exercise.model.Comment;
import exercise.repository.CommentRepository;
import exercise.model.Post;
import exercise.repository.PostRepository;
import exercise.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;


@RestController
@RequestMapping("/posts")
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    // BEGIN
    @GetMapping("/{postId}/comments")
    public Iterable<Comment> getComments(@PathVariable("postId") long id) {
        return commentRepository.findByPostId(id);
    }

    @GetMapping("/{postId}/comments/{commentId}")
    public Comment getComment(@PathVariable("postId") long postId, @PathVariable("commentId") long commentId) {
        Comment comment = commentRepository.findByIdAndPostId(commentId, postId);
        if (comment == null) {
            throw new ResourceNotFoundException("Comment not found");
        }
        return comment;
    }

    @PostMapping("/{postId}/comments")
    public void createComment(@PathVariable("postId") long id, @RequestBody Comment comment) {
        comment.setPost(postRepository.findById(id).get());
        commentRepository.save(comment);
    }

    @PatchMapping("/{postId}/comments/{commentId}")
    public Comment updateComment(@PathVariable("postId") long postId, @PathVariable("commentId") long commentId, @RequestBody Comment updatedComment) {
        Comment comment = commentRepository.findByIdAndPostId(commentId, postId);
        if (comment == null) {
            throw new ResourceNotFoundException("Comment for updating not found");
        }
        comment.setContent(updatedComment.getContent());
        return commentRepository.save(comment);
    }

    @DeleteMapping("/{postId}/comments/{commentId}") 
    public void deleteComment(@PathVariable("postId") long postId, @PathVariable("commentId") long commentId) {
        Comment comment = commentRepository.findByIdAndPostId(commentId, postId);
        if (comment == null) {
            throw new ResourceNotFoundException("Comment not exist");
        }
        commentRepository.delete(comment);
    }


    // END
}
