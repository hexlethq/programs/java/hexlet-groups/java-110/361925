package exercise;

// BEGIN
public class Point {

	private int x;
	private int y;

	public Point (final int x, final int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

}
// END
