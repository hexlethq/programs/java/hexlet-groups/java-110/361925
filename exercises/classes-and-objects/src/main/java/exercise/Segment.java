package exercise;

// BEGIN

public class Segment {

	private Point beginPoint;
	private Point endPoint;

	public Segment (final Point begin, final Point end) {
		beginPoint = begin;
		endPoint = end;
	}

	public Point getBeginPoint() {
		return beginPoint;
	}

	public Point getEndPoint() {
		return endPoint;
	}

	public Point getMidPoint() {
		int bX = beginPoint.getX();
		int eX = endPoint.getX();
		int bY = beginPoint.getY();
		int eY = endPoint.getY();

		int x =  (bX + eX) / 2;
		int y = (bY + eY) / 2;
		
		return new Point(x, y);
	}
}
// END
