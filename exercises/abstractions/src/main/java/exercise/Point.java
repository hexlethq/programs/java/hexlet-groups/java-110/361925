package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y){
        int[] point = {x,y};
        return point;
    }
    public static int getX(int[] point){
        return point[0];
    }
    public static int getY(int[] point){
        return point[1];
    }
    public static String pointToString(int[] point){
        return "("+point[0]+", "+point[1]+")";
    }
    public static int getQuadrant(int[] point){
        int x = getX(point);
        int y = getY(point);
        int result= -1;
        if ((x==0)||(y==0)) result =  0;
        if ((x>0)&&(y>0)) result = 1;
        if ((x<0)&&(y>0)) result = 2;
        if ((x<0)&&(y<0)) result = 3;
        if ((x>0)&&(y<0)) result = 4;
        return result;
    }
    // END
}
