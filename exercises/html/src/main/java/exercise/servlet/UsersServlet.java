package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import java.io.File;
import java.util.Collections;
import java.util.stream.Collectors;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        ObjectMapper mapper = new ObjectMapper();
        File json = new File("src/main/resources/users.json");
        List<Map<String, String>> users = null;
        try {
            users = mapper.readValue(json, new TypeReference<>() { });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return users;
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {

        // BEGIN
        List<Map<String,String>> users = (List<Map<String,String>>) getUsers();
        Collections.sort(users, Comparator.comparing(map -> Integer.valueOf(map.get("id"))));
        String result = users.stream()
                             .map(user -> "<tr><td>" + user.get("id") + "</td>" +
                                "<td><a href=\"\\users\\" + user.get("id") + "\\\">" +
                                user.get("firstName") + " " +  user.get("lastName") + "</a></td></tr>\n")
                             .collect(Collectors.joining());

        response.setContentType("text/html;charset=UTF-8");
        String body = """
            <!doctype html>
            <html lang=\"en\">
                <head>
                    <!-- Required meta tags -->
                    <meta charset=\"utf-8\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                    <!-- Bootstrap CSS -->
                    <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css\" 
                          rel=\"stylesheet\" 
                          integrity=\"sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3\" 
                          crossorigin=\"anonymous\">
                    <title>Пользователи</title>
                </head>
                <body>
                    <table class=\"table\">
                        <thead>
                            <tr>
                                <th scope=\"col\">id</th>
                                <th scope=\"col\">FullName</th>
                            </tr>
                        </thead>
                        <tbody>
                               %s
                        </tbody>
                    </table>
                </body>
            </html>"""
            .formatted(result);
        PrintWriter out = response.getWriter();
        out.println(body);
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {

        // BEGIN
        List<Map<String,String>> users = (List<Map<String,String>>) getUsers();
        
        if (users.stream().noneMatch(map -> map.get("id").equals(id))) {
            response.sendError(404, "Not found");
        }

        String result = users.stream()
                             .filter(map -> map.get("id").equals(id))
                             .map(map -> "<td>" + map.get("firstName") + "</td>" +
                                "<td>" + map.get("lastName") + "</td>" +
                                "<td>" + map.get("id") + "</td>" +
                                "<td>" + map.get("email") + "</td>")
                             .collect(Collectors.joining());

        response.setContentType("text/html;charset=UTF-8");
        String body = """
            <!doctype html>
            <html lang=\"en\">
                <head>
                    <!-- Required meta tags -->
                    <meta charset=\"utf-8\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                    <!-- Bootstrap CSS -->
                    <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css\" 
                          rel=\"stylesheet\" 
                          integrity=\"sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3\" 
                          crossorigin=\"anonymous\">
                    <title>Пользователи</title>
                </head>
                <body>
                    <table class=\"table\">
                        <thead>
                            <tr>
                                <th scope=\"col\">firstName</th>
                                <th scope=\"col\">lastName</th>
                                <th scope=\"col\">id</th>
                                <th scope=\"col\">email</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                               %s
                            </tr>
                        </tbody>
                    </table>
                </body>
            </html>"""
            .formatted(result);
        PrintWriter out = response.getWriter();
        out.println(body);
        // END
    }
}
