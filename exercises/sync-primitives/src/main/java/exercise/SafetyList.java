package exercise;


class SafetyList {
    // BEGIN

    private int[] arr;
    private int arrSize;
    private int size;

    public SafetyList() {
        this.arr = new int[10];
        this.arrSize = this.arr.length;
        this.size = 0;
    }

    public synchronized void  add(int newElement) {
        arr[size++] = newElement;
        if (size == (arrSize - 1)) {
            int[] buffer = arr;
            arr = new int[arrSize + arrSize /2];
            System.arraycopy(buffer, 0, arr, 0, arrSize);
            arrSize = arr.length;
        }
    }

    public synchronized int get(int index){
        return (index <= size) && (index > 0) ? arr[index] : 0;
    }

    public int getSize() {
        return size;
    }
    // END
}
