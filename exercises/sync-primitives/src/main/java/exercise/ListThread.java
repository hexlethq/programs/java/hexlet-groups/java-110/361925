package exercise;

// BEGIN
import java.util.Random;


class ListThread extends Thread {

    private SafetyList list;

    public ListThread(SafetyList newList) {
        this.list = newList;
    }

    @Override
    public void run() {
        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            try {
                Thread.currentThread().sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            list.add(random.nextInt());
        }
    }
}
// END
