package exercise;

import java.util.*;

// BEGIN
class App {

    public static LinkedHashMap<String, String> genDiff(Map<String, Object> data1, Map<String, Object> data2) {
        LinkedHashMap<String, String> result = new LinkedHashMap<>();
        Set<String> buffer = new TreeSet<>(Comparator.naturalOrder());
        buffer.addAll(data1.keySet());
        buffer.addAll(data2.keySet());
        for (String s : buffer) {
            if (!data1.containsKey(s) && data2.containsKey(s)) {
                result.put(s, "added");
            } else if (data1.containsKey(s) && !data2.containsKey(s)) {
                result.put(s, "deleted");
            } else if (data1.containsKey(s) && data2.containsKey(s) && !data1.get(s).equals(data2.get(s))) {
                result.put(s, "changed");
            } else if (data1.containsKey(s) && data2.containsKey(s) && data1.get(s).equals(data2.get(s))) {
                result.put(s, "unchanged");
            }
        }
        return result;
    }
}
//END
