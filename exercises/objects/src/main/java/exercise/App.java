package exercise;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

class App {
    // BEGIN
    public static String buildList(String[] data){
        StringBuilder result = new StringBuilder("");
        if (data.length>0) {
            result.append("<ul>\n");
            for (String string : data
            ) {
                result.append("  <li>");
                result.append(string);
                result.append("</li>\n");
            }
            result.append("</ul>");
        }
        return result.toString();
    }

    public static String getUsersByYear(String[][] users, int year){
        StringBuilder result = new StringBuilder("");

        for (String[] entry: users
        ) {
            if (LocalDate.parse(entry[1]).getYear() == year) {
                result.append("  <li>");
                result.append(entry[0]);
                result.append("</li>\n");
            }
        }
        if (result.length()>0) {
            result.append("</ul>");
            result.insert(0, "<ul>\n");
        }
        return result.toString();
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
   /*
   public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN

        StringBuilder result = new StringBuilder("");
        Date inputDate = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH).parse(date);
            for (String[] entry: users
            ) {
                Date birthday = new SimpleDateFormat("yyyy-MM-dd").parse(entry[1]);
                if ( inputDate.after(birthday)) {
                    result.append(entry[0]);
                    result.append("\n");
                }
            }
        return result.toString();

        // END
    }
    */
}
