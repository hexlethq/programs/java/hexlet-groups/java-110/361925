package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String phrase) {
        String temp = phrase.trim();
        String abbreviation = temp.substring(0, 1);

        for (int i = 1; i < (temp.length() - 1); i++) {
            char currentChar = temp.charAt(i);
            char nextChar = temp.charAt(i + 1);
            if (Character.isWhitespace(currentChar) && !(Character.isWhitespace(nextChar))) {
                abbreviation += nextChar;
            }
        }
        abbreviation = abbreviation.toUpperCase();
        return abbreviation;
    }
    // END
}
