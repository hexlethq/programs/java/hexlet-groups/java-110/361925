package exercise;

import java.lang.reflect.Proxy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// BEGIN
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomBeanPostProcessor.class);
    private Map<String, String> inspectingBeans = new HashMap<>();

    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        Inspect inspectAnnotation = bean.getClass().getAnnotation(Inspect.class);
        if(inspectAnnotation != null) {
            Map<String, String> inspectingBean = Map.of(beanName, inspectAnnotation.level());
            inspectingBeans.putAll(inspectingBean);
        }
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) {
        
        if (inspectingBeans.containsKey(beanName)) {
            inspectingBeans.forEach( (k,v) -> LOGGER.info("====== " + k + " log_level:" + v + " ======"));
            Object originalBean = bean;
            InvocationHandler handler = new InvocationHandler() {

                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    LOGGER.info("method invoked");
                    switch (inspectingBeans.get(beanName)) {
                        case "info" -> LOGGER.info("Was called method: {}() with arguments: {}", method.getName(), args);
                        case "debug" -> LOGGER.debug("Was called method: {}() with arguments: {}", method.getName(), args);
                        default -> throw new Exception("log level exception");
                    }
                    return method.invoke(originalBean, args);
                }

            };
            LOGGER.info("====== handler created =======");
            bean = Proxy.newProxyInstance(
                bean.getClass().getClassLoader(),
                bean.getClass().getInterfaces(),
                handler);
            LOGGER.info("====== proxy created ======= ");    
        };
        return bean;
    }
}
// END
