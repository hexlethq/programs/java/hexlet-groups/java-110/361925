package exercise.service;

import exercise.HttpClient;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import exercise.CityNotFoundException;
import exercise.repository.CityRepository;
import exercise.model.City;
import org.springframework.beans.factory.annotation.Autowired;


@Service
public class WeatherService {

    @Autowired
    CityRepository cityRepository;

    // Клиент
    HttpClient client;

    // При создании класса сервиса клиент передаётся снаружи
    // В теории это позволит заменить клиент без изменения самого сервиса
    WeatherService(HttpClient client) {
        this.client = client;
    }

    // BEGIN
    public Map<String, String> getCityWeather(String cityName) {
        Map<String, String> weather = null;
        ObjectMapper mapper = new ObjectMapper();
        String response = client.get("http://weather/api/v2/cities/" + cityName);
        if (response.length() == 0) {
            throw new CityNotFoundException("City with this name not found");
        }
        try {
            weather = mapper.readValue(response, Map.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return weather;
    }

    public Map<String, String> getWeatherParam(String cityName, String paramName) {
        String paramValue = getCityWeather(cityName).get(paramName);
        return Map.of(paramName, paramValue, "name", cityName);
    }
    // END
}
