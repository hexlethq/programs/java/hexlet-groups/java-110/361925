package exercise.controller;
import exercise.model.City;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping(path = "/cities/{id}")
    public Map<String, String> getWeather(@PathVariable long id) {
        City city = cityRepository.findById(id).get();
        return weatherService.getCityWeather(city.getName());
    }

    @GetMapping(path = "/search")
    public List<Map<String, String>> getCitiesTemperature(@RequestParam(name = "name", defaultValue = "") String prefix) {
        List<City> searchResult;
        if (prefix.isBlank()) {
            searchResult = cityRepository.findAll();
        }
        searchResult = cityRepository.findByNameStartingWithIgnoreCase(prefix);
        return searchResult.stream()
            .map(city -> city.getName())
            .map(cityName -> weatherService.getWeatherParam(cityName, "temperature"))
            .sorted((city1, city2) -> city1.get("name").compareTo(city2.get("name")))
            .collect(Collectors.toList())
            ;

    }
}

    // END

