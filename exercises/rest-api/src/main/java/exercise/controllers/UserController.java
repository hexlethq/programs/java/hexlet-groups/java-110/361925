package exercise.controllers;

import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;
import java.util.List;

import exercise.domain.User;
import exercise.domain.query.QUser;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        // BEGIN
        String json = DB.json().toJson(new QUser().findList());
        ctx.json(json);
        // END
    };

    public void getOne(Context ctx, String id) {

        // BEGIN
        String json = DB.json().toJson(new QUser().id.equalTo(Long.parseLong(id)).findOne());
        ctx.json(json);
        // END
    };

    public void create(Context ctx) {

        // BEGIN
        User user = ctx.bodyValidator(User.class)
            .check(body -> !body.getFirstName().isBlank(), "Имя не может быть пустым")
            .check(body -> !body.getLastName().isBlank(), "Фамилия не может быть пустой")
            .check(body -> EmailValidator.getInstance().isValid(body.getEmail()), "Некорректный email")
            .check(body -> body.getPassword().length() > 3, "Пароль должен содержать минимум 4 символа")
            .check(body -> StringUtils.isNumeric(body.getPassword()), "Пароль должен состоять только из цифр")
            .get();
        user.save();
        // END
    };

    public void update(Context ctx, String id) {
        // BEGIN
        User user = DB.json().toBean(User.class, ctx.body());
        user.setId(id);
        user.update();
        // END
    };

    public void delete(Context ctx, String id) {
        // BEGIN
        new QUser().id.equalTo(Long.parseLong(id)).delete();
        // END
    };
}
