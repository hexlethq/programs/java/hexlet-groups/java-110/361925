package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;

// BEGIN
class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> bookList, Map<String, String> dict) {
        List<Map<String, String>> result = new ArrayList<>();
        for (Map map : bookList) {
            boolean checkMatches = true;
            for (Entry dictEnrty : dict.entrySet()) {
                if (!map.containsKey(dictEnrty.getKey()) || !map.containsValue(dictEnrty.getValue())
                        || result.contains(map)) {
                    checkMatches = false;
                }
            }
            if (checkMatches) {
                result.add(map);
            }
        }
        return result;
    }
}

//END
