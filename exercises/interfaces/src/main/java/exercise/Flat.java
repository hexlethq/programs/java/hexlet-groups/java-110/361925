package exercise;

// BEGIN
public class Flat implements Home {

	private double area;
	private int floor;

	public Flat(double area, double balconyArea, int floor) {
        this.area = area + balconyArea;
        this.floor = floor;
	}

	public String toString() {
		return String.format("Квартира площадью %.1f метров на %d этаже", area, floor);
	}

	public int compareTo(Home another) {
		return area > another.getArea() ? 1 : area < another.getArea() ? -1 : 0;
	}

    public double getArea() {
    	return this.area;
    }
}
// END
