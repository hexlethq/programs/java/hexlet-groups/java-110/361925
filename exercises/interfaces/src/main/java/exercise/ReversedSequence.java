package exercise;

// BEGIN
public class ReversedSequence implements CharSequence {

	private String content;

	public ReversedSequence(final String content) {
			this.content = new StringBuilder(content).reverse().toString();
	}

	public char charAt(final int index) {
        return content.charAt(index);
	}

	public int length() {
        return content.length();
	}

	public CharSequence subSequence(final int start, final int end) {
        return content.subSequence(start, end);
	}

	public String toString() {
        return content;
	}
}
// END
