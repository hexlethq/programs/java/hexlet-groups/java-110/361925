package exercise;

// BEGIN
public class Cottage implements Home {

	private double area;
	private int floorCount;

	public Cottage(double area, int floors) {
		this.area = area;
		this.floorCount = floors;
	}

	public String toString() {
		return String.format("%d этажный коттедж площадью %.1f метров", floorCount, area);
	}

	public int compareTo(Home another) {
		return area > another.getArea() ? 1 : area < another.getArea() ? -1 : 0;
	}

	public double getArea() {
		return this.area;
	}
}
// END
