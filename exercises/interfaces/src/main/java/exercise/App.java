package exercise;

import java.util.List;
import java.util.stream.Collectors;
import java.util.ArrayList;

// BEGIN
import java.util.Comparator;

public class App {
	public static List<String> buildAppartmentsList(final List<Home> homes, final int quantity) {
        return homes.stream()
            .sorted(Comparator.comparingDouble(home -> home.getArea()))
            .limit(quantity)
            .map(home -> home.toString())
            .collect(Collectors.toList());
	}
} //.....
// END
