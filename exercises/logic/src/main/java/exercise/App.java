package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable=false;
        if (((number % 2)==1)&&(number>=1001)) {
            isBigOddVariable = true;
        }
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        System.out.println( ((number % 2)!=1) ? "yes" : "no");
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        if ((0<=minutes)&&(minutes<=14)) {
            System.out.println("First");
        } else if ((15<=minutes)&&(minutes<=30)) {
            System.out.println("Second");
        } else if ((31<=minutes)&&(minutes<=45)) {
            System.out.println("Third");
        } else if ((46<=minutes)&&(minutes<=59)) {
            System.out.println("Fourth");
        } else {
            System.out.println("Введите число от 0 до 59");
        }
            // END

    }
}
