package exercise;

import java.time.LocalDateTime;

import exercise.daytimes.Daytime;
import exercise.daytimes.Morning;
import exercise.daytimes.Day;
import exercise.daytimes.Evening;
import exercise.daytimes.Night;

// BEGIN
import java.time.LocalDateTime;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;

@Configuration
public class MyApplicationConfig {

    @Bean
    public Daytime myBean() {
        Daytime currDayTime = null;
        int currentHour = LocalDateTime.now().getHour();
        if (currentHour >= 6 && currentHour < 12) {
            currDayTime = new Morning();
        } else if (currentHour >= 12 && currentHour < 18) {
            currDayTime = new Day();
        } else if (currentHour >= 18 && currentHour < 23) {
            currDayTime = new Evening();
        } else {
            currDayTime = new Night();
        }
        return currDayTime;
    }
}
// END
