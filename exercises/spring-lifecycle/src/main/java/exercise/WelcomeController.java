package exercise;

import org.springframework.web.bind.annotation.GetMapping;



// BEGIN
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import exercise.daytimes.Daytime;

@RestController
@RequestMapping("/daytime")
class WelcomeController {

    @Autowired
    Meal meal;

    @Autowired
    Daytime daytime;

    @GetMapping(path = "") 
    public String wish() {
        return "It is " + daytime.getName() + " now. Enjoy your " + meal.getMealForDaytime(daytime.getName());
    }
    
}
// END
