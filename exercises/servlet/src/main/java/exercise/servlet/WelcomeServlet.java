package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
// import javax.servlet.annotation.WebServlet;

// BEGIN
//@WebServlet("/")
public class WelcomeServlet extends HttpServlet {

	@Override
	protected void doGet (HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
			response.setContentType("text/plain");
       		PrintWriter printWriter = response.getWriter();
       		//printWriter.println("Hello, Hexlet!");
       		printWriter.write("Hello, Hexlet!");
       		printWriter.close();
	}

}
// END
