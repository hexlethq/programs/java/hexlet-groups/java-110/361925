package exercise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.*;

// BEGIN
public class App {
    public static boolean scrabble(String string, String word) {
        List<Character> listString = new ArrayList<>();
        for (char c : string.toLowerCase().toCharArray()) {
            listString.add(c);
        }
        List<Character> listWord = new ArrayList<>();
        for (char c : word.toLowerCase().toCharArray()) {
            listWord.add(c);
        }
        for (Character c: listString
             ) {
            listWord.remove(c);
        }

        return listWord.size() > 0 ? false : true;
    }
}
//END
