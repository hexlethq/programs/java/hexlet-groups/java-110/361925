package exercise.controllers;

import io.javalin.http.Handler;
import java.util.List;
import java.util.Map;
import io.javalin.core.validation.Validator;
import io.javalin.core.validation.ValidationError;
import io.javalin.core.validation.JavalinValidation;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

import exercise.domain.User;
import exercise.domain.query.QUser;

public final class UserController {

    public static Handler listUsers = ctx -> {

        List<User> users = new QUser()
            .orderBy()
                .id.asc()
            .findList();

        ctx.attribute("users", users);
        ctx.render("users/index.html");
    };

    public static Handler showUser = ctx -> {
        long id = ctx.pathParamAsClass("id", Long.class).getOrDefault(null);

        User user = new QUser()
            .id.equalTo(id)
            .findOne();

        ctx.attribute("user", user);
        ctx.render("users/show.html");
    };

    public static Handler newUser = ctx -> {

        ctx.attribute("errors", Map.of());
        ctx.attribute("user", Map.of());
        ctx.render("users/new.html");
    };

    public static Handler createUser = ctx -> {
        // BEGIN
        Validator<String> firstNameVld = ctx.formParamAsClass("firstName", String.class)
            .check(firstName -> !firstName.isEmpty(), "Поле не должно быть пустым");
        Validator<String> lastNameVld = ctx.formParamAsClass("lastName", String.class)
            .check(lastName -> !lastName.isEmpty(), "Поле не должно быть пустым");
        Validator<String> emailVld = ctx.formParamAsClass("email", String.class)
            .check(email -> EmailValidator.getInstance().isValid(email), "Некорректный емейл");
        Validator<String> passVld = ctx.formParamAsClass("password", String.class)
            .check(pass -> StringUtils.isNumeric(pass), "Пароль должен содержать только цифры")
            .check(pass -> pass.length() > 3, "Пароль должен содержать минимум 4 символа");

        Map<String, List<ValidationError<? extends Object>>> errors = JavalinValidation.collectErrors(
            firstNameVld, lastNameVld, emailVld, passVld
            );
        User user = new User(
            ctx.formParam("firstName"),
            ctx.formParam("lastName"),
            ctx.formParam("email"),
            ctx.formParam("password")
            );
        ctx.attribute("user", user);
        ctx.attribute("errors", errors);
        

        if(!errors.isEmpty()) {
            ctx.status(422);
            ctx.render("users/new.html");
            return;
        }

        user.save();
        ctx.sessionAttribute("flash", "Пользователь успешно создан");
        ctx.redirect("/users");
        // END
    };
}
