package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String[][] enlargeArrayImage(String[][] image) {
        return Stream.of(image)
                .flatMap(x -> Stream.of(x, x))
                .map(y -> Stream.of(y)
                        .flatMap(z -> Stream.of(z, z))
                        .toArray(String[]::new))
                .toArray(String[][]::new);
    }
}
// END
