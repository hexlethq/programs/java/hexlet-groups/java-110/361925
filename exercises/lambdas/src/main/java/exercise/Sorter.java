package exercise;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.List;
import java.time.LocalDate;
import java.util.stream.Collectors;

//
//// BEGIN
class Sorter {
    public static List<String> takeOldestMans(List<Map<String, String>> usersList) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return usersList.stream()
                .filter(z -> z.get("gender").equals("male"))
                .sorted((o1, o2) -> {
                    try {
                        Date first = format.parse(o1.get("birthday"));
                        Date second = format.parse(o2.get("birthday"));
                        return first.compareTo(second);
                    } catch (Exception e) {
                        return 0;
                    }
                })
                .map(x -> x.get("name"))
                .collect(Collectors.toList());
    }
}
//// END
