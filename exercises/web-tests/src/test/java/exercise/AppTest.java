package exercise;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import java.beans.Transient;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import io.javalin.Javalin;
import io.ebean.DB;
import io.ebean.Transaction;

import exercise.domain.User;
import exercise.domain.query.QUser;

class AppTest {

    private static Javalin app;
    private static String baseUrl;
    private static Transaction transaction;

    // BEGIN
    @BeforeAll
    static void beforeAll() {
        app = App.getApp();
        app.start(0);
        int port = app.port();
        baseUrl = "http://localhost:" + port;
    }

    @AfterAll
    static void afterAll() {
        app.stop();
    }
    // END

    // Хорошей практикой является запуск тестов с базой данных внутри транзакции.
    // Перед каждым тестом транзакция открывается,
    @BeforeEach
    void beforeEach() {
        transaction = DB.beginTransaction();
    }

    // А после окончания каждого теста транзакция откатывается
    // Таким образом после каждого теста база данных возвращается в исходное состояние,
    // каким оно было перед началом транзакции.
    // Благодаря этому тесты не влияют друг на друга
    @AfterEach
    void afterEach() {
        transaction.rollback();
    }

    @Test
    void testRoot() {
        HttpResponse<String> response = Unirest.get(baseUrl).asString();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void testUsers() {

        // Выполняем GET запрос на адрес http://localhost:port/users
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users")
            .asString();
        // Получаем тело ответа
        String content = response.getBody();

        // Проверяем код ответа
        assertThat(response.getStatus()).isEqualTo(200);
        // Проверяем, что на станице есть определенный текст
        assertThat(content).contains("Wendell Legros");
        assertThat(content).contains("Larry Powlowski");
    }

    @Test
    void testUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/5")
            .asString();
        String content = response.getBody();

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(content).contains("Rolando Larson");
        assertThat(content).contains("galen.hickle@yahoo.com");
    }

    @Test
    void testNewUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/new")
            .asString();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    // BEGIN
    @Test
    void testCreateUser() {
        HttpResponse<String> response = Unirest.post(baseUrl + "/users")
        .header("accept", "application/json")
        .field("firstName", "Alex")
        .field("lastName", "Alexandrov")
        .field("email", "alex@mail.ru")
        .field("password", "1234")
        .asString();

        User user = new QUser()
            .email.equalTo("alex@mail.ru")
            .findOne();
        
        assertThat(response.getStatus()).isEqualTo(302);
        assertThat(user).isNotNull();
        assertThat(user.getFirstName()).isEqualTo("Alex");
        assertThat(user.getLastName()).isEqualTo("Alexandrov");
        assertThat(user.getEmail()).isEqualTo("alex@mail.ru");
        assertThat(user.getPassword()).isEqualTo("1234");
    }

    @Test
    void testCreateUser_negative() {
        HttpResponse<String> response = Unirest.post(baseUrl + "/users")
            .header("accept", "application/json")
            .field("firstName", "")
            .field("lastName", "")
            .field("email", "abc")
            .field("password", "12")
            .asString();
        
        User user = new QUser()
            .email.equalTo("abc")
            .password.equalTo("12")
            .findOne();


        assertThat(response.getStatus()).isEqualTo(422);
        assertThat(user).isNull();
        assertThat(response.getBody()).contains("abc");
        assertThat(response.getBody()).contains("12");

        assertThat(response.getBody()).contains("Имя не должно быть пустым");
        assertThat(response.getBody()).contains("Фамилия не должна быть пустой");
        assertThat(response.getBody()).contains("Должно быть валидным email");
        assertThat(response.getBody()).contains("Пароль должен содержать не менее 4 символов");
    }
    // END
}
