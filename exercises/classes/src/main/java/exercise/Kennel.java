package exercise;

import java.util.Arrays;


// BEGIN
public class Kennel{
    private static int count = 0;
    private static String[][] records = null;

    public static void addPuppy(String[] puppy) {
        String[][] newRecord = new String[1][2];
        String[][] oldRecords;
        if (records == null) {
            oldRecords = new String[0][0];
        } else {
            oldRecords = records;
        }
        newRecord[0][0] = puppy[0];
        newRecord[0][1] = puppy[1];
        records = new String[oldRecords.length + 1][2];

        for (int i = 0; i < records.length; i++) {
            if (i< oldRecords.length) {
                records[i] = oldRecords[i];
            } else {
                records[i]= newRecord[0];
            }
        }
        count++;
    }
    public static void addSomePuppies(String[][] somePuppies){
        for (String[] puppy: somePuppies
        ) {
            addPuppy(puppy);
        }
    }
    public static int getPuppyCount(){
        return count;
    }
    public static boolean isContainPuppy(String findingName) {
        boolean result = false;
        for (String[] entry: records
        ) {
            if (entry[0].equals(findingName)) result = true;
        }
        return result;
    }
    public static String[][] getAllPuppies() {
        return records;
    }
    public static String[] getNamesByBreed(String breed){
        String[][] list = getAllPuppies();
        String buffer = "";
        for (String[] entry: list
        ) {
            if (entry[1].equals(breed)) {
                buffer += entry[0]+"\n";
            }
        }
        String[] result = buffer.split("\n") ;
        return result;
    }
    public static void resetKennel(){
        count = 0;
        records = null;
    }
}
// END
