package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr){
        int result = -1;
        int temp = Integer.MIN_VALUE;
        if (arr!=null) {
            for (int i = 0; i < arr.length; i++) {
                if ((arr[i] > temp)&&(arr[i]<0)) {
                    temp = arr[i];
                    result = i;
                }
            }
        }
        return result;
    }

    public static int[] getElementsLessAverage(int[] arr){
        int[] result = arr == null ? null : new int[arr.length];
        if (arr!=null) {
            double average = 0;
            for (int i : arr
            ) {
                average += (double) i;
            }
            average = average / (double) arr.length;

            int counter = 0;
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] <= average) {
                    result[counter] = arr[i];
                    counter++;
                }
            }
            int[] buffer = Arrays.copyOf(result, counter);
            //buffer = Arrays.copyOf(result, counter);
            result = buffer;
        }

        return result;

    }
    // END
}
