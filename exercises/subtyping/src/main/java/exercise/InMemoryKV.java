package exercise;

import java.util.Map;
import java.util.HashMap;

// BEGIN
public class InMemoryKV implements KeyValueStorage {

	private Map<String, String> content;

	public InMemoryKV (Map<String, String> map) {
		content = new HashMap<>(map);
	}

	public void set(String key, String value) {
        content.put(key, value);
	}

    public void unset(String key) {
    	content.remove(key);
    }
    public String get(String key, String defaultValue) {
    	return content.getOrDefault(key, defaultValue);
    }

    public Map<String, String> toMap() {
    	return new HashMap<>(content);
    }

}
// END
