package exercise;

import java.util.Map;
import java.util.HashMap;

// BEGIN
public class FileKV implements KeyValueStorage {

	private String path;

	public FileKV(final String path, final Map<String, String> map) {
        this.path = path;
		saveMap(map);
	}

	public void set(String key, String value) {
		Map<String, String> buffer = getMap();
		buffer.put(key, value);
		saveMap(buffer);
	}

    public void unset(String key) {
    	Map<String, String> buffer = getMap();
		buffer.remove(key);
    	saveMap(buffer);

    }
    public String get(String key, String defaultValue) {
    	return getMap().getOrDefault(key, defaultValue);
    }

    public Map<String, String> toMap() {
    	return new HashMap<>(getMap());
    }

    private void saveMap(Map<String, String> map) {
        Utils.writeFile(path, Utils.serialize(map));
    }

    private Map<String, String> getMap() {
         return Utils.unserialize(Utils.readFile(path));
    }

}
// END
