package exercise;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.HashMap;

// BEGIN
public class App {

	public static void swapKeyValue(KeyValueStorage storage) {
		Map<String, String> buffer = new HashMap<>();
		for(Map.Entry<String, String> entry : storage.toMap().entrySet()) {
			buffer.put(entry.getValue(), entry.getKey());
			storage.unset(entry.getKey());
		}
		for(Map.Entry<String, String> entry : buffer.entrySet()) {
			storage.set(entry.getKey(), entry.getValue());
		}
	}
}
// END
