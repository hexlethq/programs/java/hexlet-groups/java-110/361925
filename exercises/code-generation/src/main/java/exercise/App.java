package exercise;

import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

// BEGIN
import java.io.IOException;

class App {

	public static void save(Path path, Car car) {
		try {
			Files.write(path, car.serialize().getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Car extract(Path path) {
		Car car = null;
		try {
			car = Car.unserialize(Files.readString(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return car;
	}
}
// END
