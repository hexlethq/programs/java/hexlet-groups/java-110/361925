package exercise;
import java.util.List;
import java.util.ArrayList;

// BEGIN
import exercise.connections.*;

public class TcpConnection {

    private String ip;
    private int port;
    private Connection state;
    
	public TcpConnection(String ip, int port) {
		this.ip = ip;
		this.port = port;
		state = new Disconnected(this);
	}

	public String getCurrentState() {
		return state.getCurrentState();
	}

	public void connect() {
		state.connect();
	}

	public void disconnect() {
		state.disconnect();
	}

	public void write(String msg) {
		state.write(msg);
	}

	public void changeState(Connection state) {
		this.state = state;
	}


}
// END
