package exercise.connections;

// BEGIN
import exercise.TcpConnection;

public class Connected implements Connection {
	
	private	final TcpConnection link;

	public Connected(TcpConnection link) {
		this.link = link;
	}

	public void connect() {
		System.out.println("Error. Try to connect when connected");
	}
    
    public void disconnect() {
    	link.changeState(new Disconnected(link));
    }

    public String getCurrentState() {
    	return "connected";
    }

    public void write(String msg) {
    	System.out.println(msg);
    }
}
// END
