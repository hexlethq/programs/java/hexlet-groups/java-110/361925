package exercise.connections;

// BEGIN
import exercise.TcpConnection;

public class Disconnected implements Connection {

    private	final TcpConnection link;

	public Disconnected(TcpConnection link) {
		this.link = link;
	}

	public void connect() {
		link.changeState(new Connected(link));
	}

    public void disconnect() {
    	System.out.println("Error. Try to disconnect when disconnected");
    }
    
    public String getCurrentState() {
    	return "disconnected";
    }

    public void write(String msg) {
    	System.out.println("Error. You are disconnected");
    }
}
// END
