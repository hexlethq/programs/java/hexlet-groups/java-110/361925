package exercise;

class Converter {
    // BEGIN
    public static void main(String[] args) {
        System.out.println(("10 Kb = "+convert(10, "b"))+" b");

    }
    public static int convert(int size, String convDirection){
        int result;
        switch (convDirection) {
            case ("b"):
                result = size * 1024;
                break;
            case ("Kb"):
                result = size / 1024;
                break;
            default:
                result = 0;
        }
        return result;
    }
    // END
}
