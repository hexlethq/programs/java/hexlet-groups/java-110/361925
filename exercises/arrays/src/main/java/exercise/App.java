package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {
        int[] result = new int[arr.length];
        int j=0;
        for (int i = (arr.length-1); i >= 0 ; i--) {
            result[j]= arr[i];
            j++;
        }
        return result;

    }

    public static int mult(int[] arr) {
        int result =1;
        for (int i: arr
        ) {
            result = result*i;
        }
        return result;
    }

    // END
}
