package exercise;

import java.lang.reflect.Field;

// BEGIN
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class Validator {

	public static List<String> validate(Address adr) {
		List<String> result = new ArrayList<>();
		Field[] fields = adr.getClass().getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			try {
			    if (field.getAnnotation(NotNull.class) != null
				    && field.get(adr) == null) {
				    result.add(field.getName());
			    }
			} catch (Exception e) {
			e.printStackTrace();
			}
		}
		return result;
	}

	public static Map<String, List<String>> advancedValidate(Address adr) {
        Map<String, List<String>> result = new HashMap<>();
        String fieldValue = "";
        Field[] fields = adr.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                fieldValue = (String) field.get(adr);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (!analyse(field, fieldValue).isEmpty()) {
                result.putAll(analyse(field, fieldValue));
            }
        }
        return result;
	}

	private static Map<String, List<String>> analyse(Field field, String fieldValue) {
        List<String> errors = new ArrayList<>();
        boolean isNotNullExist = field.getAnnotation(NotNull.class) != null;
        boolean isMinLengthExist = field.getAnnotation(MinLength.class) != null;
        int minLen = -1;
        if (isMinLengthExist) {
            minLen = field.getAnnotation(MinLength.class).minLength();
        }
        if (fieldValue == null) {
            if (isNotNullExist) {
                errors.add("can not be null");
            }
        } else if (minLen > 0 && minLen > fieldValue.length()) {
            errors.add(String.format("length less than %d", minLen));
        }
        return  errors.isEmpty() ? Map.of() : Map.of(field.getName(), errors);
    }
}
// END
