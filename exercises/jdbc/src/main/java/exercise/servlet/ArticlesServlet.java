package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import org.apache.commons.lang3.ArrayUtils;

import exercise.TemplateEngineUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;



public class ArticlesServlet extends HttpServlet {

    private String getId(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return null;
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 1, null);
    }

    private String getAction(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return "list";
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 2, getId(request));
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "list":
                showArticles(request, response);
                break;
            default:
                showArticle(request, response);
                break;
        }
    }

    private void showArticles(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        List<Map<String, String>> articles = new ArrayList<>();
        int page = 1;
        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        page = page > 0 ? page : 1;
        int offset = (page - 1) * 10;
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT id, title FROM articles ORDER BY id LIMIT 10 OFFSET ?;");
            ps.setInt(1, offset);
            ResultSet rs = ps.executeQuery();
            rs.first();
            do {
                articles.add(Map.of("id", rs.getNString("id"), 
                                "title", rs.getNString("title")));
            } while (rs.next());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (articles.size() == 0) {
            page--;
            response.sendRedirect("/articles?page=" + page);
        }
        request.setAttribute("articles", articles);
        request.setAttribute("page", page);
        // END
        TemplateEngineUtil.render("articles/index.html", request, response);
    }

    private void showArticle(HttpServletRequest request,
                         HttpServletResponse response)
                 throws IOException, ServletException {
                    
        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        String title = "title";
        String body = "body";
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT title, body FROM articles WHERE id = ?;");
            Long id = Long.parseLong(getId(request));
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            rs.first();
            title = rs.getNString("title");
            body = rs.getNString("body");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (title == "title") {
            response.sendError(404);
        }
        request.setAttribute("title", title);
        request.setAttribute("body", body);
        // END
        TemplateEngineUtil.render("articles/show.html", request, response);
    }
}
