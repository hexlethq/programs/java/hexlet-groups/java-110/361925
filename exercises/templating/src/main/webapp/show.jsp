<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<%@ page import ="java.util.Map"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" 
    crossorigin="anonymous">

    <title>user's info</title>
  </head>
  <body>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">id</th>
            <th scope="col">First name</th>
            <th scope="col">Last name</th>
            <th scope="col">email</th>
          </tr>
        </thead>
        <tbody>
          <% Map<String, String> user = (Map<String, String>) request.getAttribute("user"); %>
            <tr>
              <td><%= user.get("id") %></td>
              <td><%= user.get("firstName") %></td>
              <td><%= user.get("lastName") %></td>
              <td><%= user.get("email") %></td>
            </tr>
        </tbody>
      </table>
  </body>
</html>
<!-- END -->
