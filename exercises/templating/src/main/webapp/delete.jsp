<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<%@ page import ="java.util.Map"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" 
    crossorigin="anonymous">

    <title>Delete user</title>
  </head>
  <body>
      <% Map<String, String> user = (Map<String, String>) request.getAttribute("user"); %>
    <form action="/users/delete?id=<%=user.get("id")%>" method="post">
        <h3>Delete user "<%= user.get("firstName") %> <%= user.get("lastName") %>"?</h3>
        <input type="submit" value="Delete">
    </form>
  </boDy>
</html>
<!-- END -->
