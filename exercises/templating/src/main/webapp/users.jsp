<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<%@ page import ="java.util.List"%>
<%@ page import ="java.util.Map"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" 
    crossorigin="anonymous">

    <title>All users</title>
  </head>
  <body>
    <table class="table table-success table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First name</th>
            <th scope="col">Last name</th>
          </tr>
        </thead>
        <tbody>
          <%
          List<Map<String, String>> users = (List<Map<String, String>>) request.getAttribute("users");
          for (Map<String, String> map : users) { %>
            <tr>
              <th><a href="/users/delete?id=<%=map.get("id")%>">delete</a></th>
              <td><a href="/users/show?id=<%=map.get("id")%>" class="link-primary"> <%= map.get("firstName") %> </a></td>
              <td><a href="/users/show?id=<%=map.get("id")%>" class="link-primary"> <%= map.get("lastName") %> </a></td>
            </tr>
          <% } %>
        </tbody>
      </table>
  </body>
</html>
<!-- END -->
