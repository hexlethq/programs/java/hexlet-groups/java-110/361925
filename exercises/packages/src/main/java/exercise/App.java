// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

import java.util.Arrays;

public class App{

    public static double[] getMidpointOfSegment(double[][] segment){
    double[] point =
            Point.makePoint(
    (Point.getX(Segment.getBeginPoint(segment))+Point.getX(Segment.getEndPoint(segment)))/2,
    (Point.getY(Segment.getBeginPoint(segment))+Point.getY(Segment.getEndPoint(segment)))/2
    );
    return point;
    }
    public static double[][] reverse(double[][] segment){
        double[][] reverseSegment =
                Segment.makeSegment(
                        
                        Segment.getEndPoint(segment).clone(),
                        Segment.getBeginPoint(segment).clone()
                );

        return reverseSegment;
    }

}



// END
