// BEGIN
package exercise.geometry;

        public class Segment{
            public static double[][] makeSegment(double[] point1, double[] point2) {
            double[][] segment = {point1, point2};
            return segment;
            }
            public static double[] getBeginPoint(double[][] segment) {
            double[] point = segment[0];
            return point;
            }
            public static double[] getEndPoint(double[][] segment) {
                double[] point = segment[1];
                return point;
            }

        }

// END
