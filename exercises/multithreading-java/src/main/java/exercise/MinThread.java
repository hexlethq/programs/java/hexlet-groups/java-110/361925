package exercise;

import java.lang.Thread;
import java.util.logging.Logger;
import java.util.logging.Level;

// BEGIN
public class MinThread extends Thread {

    private int[] arr;
    private int minValue;

    public MinThread(int[] input) {
        this.arr = input;
    }
    
    @Override
    public void run() {
        this.minValue = arr[0];
        for (int element : arr) {
            if (element < minValue) {
                minValue = element;
            }
        }
    }

    public int getMinValue() {
        return this.minValue;
    }
}
// END
