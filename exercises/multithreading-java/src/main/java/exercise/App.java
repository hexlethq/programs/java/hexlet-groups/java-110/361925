package exercise;

import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

class App {
    private static final Logger LOGGER = Logger.getLogger("AppLogger");

    // BEGIN
    public static Map<String, Integer> getMinMax(int[] input) {
        MinThread minThread = new MinThread(input);
        MaxThread maxThread = new MaxThread(input);

        minThread.start();
        LOGGER.info("Thread minThread started");
        maxThread.start();
        LOGGER.info("Thread maxThread started");

        try {
            minThread.join();
            LOGGER.info("Thread minThread finished");
            maxThread.join();
            LOGGER.info("Thread maxThread finished");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return Map.of("min", minThread.getMinValue(), "max", maxThread.getMaxValue());
    }
    // END
}

