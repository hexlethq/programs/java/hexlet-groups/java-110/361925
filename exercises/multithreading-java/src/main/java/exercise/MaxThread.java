package exercise;

import java.lang.Thread;
import java.util.logging.Logger;
import java.util.logging.Level;

// BEGIN
public class MaxThread extends Thread {

    private int[] arr;
    private int maxValue;

    public MaxThread(int[] input) {
        this.arr = input;
    }

    @Override
    public void run() {
        this.maxValue = arr[0];
        for (int element : arr) {
            if (element > maxValue) {
                maxValue = element;
            }
        }
    }

    public int getMaxValue() {
        return this.maxValue;
    }
}
// END
