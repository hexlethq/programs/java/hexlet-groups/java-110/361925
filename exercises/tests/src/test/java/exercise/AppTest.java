package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List<Integer> testList = Arrays.asList(1,2,3,4,5);
        int testCount = 3;
        List<Integer> expected = Arrays.asList(1,2,3);
        assertThat(expected).isEqualTo(App.take (testList, testCount));
        // END
    }
}
