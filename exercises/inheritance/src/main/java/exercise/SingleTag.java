package exercise;

import java.util.Map;

// BEGIN
public class SingleTag extends Tag {

	public SingleTag(final String name, final Map<String, String> attributes) {
		super(name, attributes);
	}

	public String toString() {
		return tag;
	}
}
// END
