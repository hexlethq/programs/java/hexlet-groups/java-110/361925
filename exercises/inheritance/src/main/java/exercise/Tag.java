package exercise;

import java.util.stream.Collectors;
import java.util.Map;

// BEGIN
import java.util.HashMap;

public class Tag {
	
	protected  final String tag;

	protected final String name;

	protected  final Map<String, String> attributes;

	protected Tag(final String name, final Map<String, String> attributes) {
		this.name = name;
		this.attributes = new HashMap<>(attributes);
		tag = String.format("<%s%s>", name, format(attributes));
	}

	private String format(final Map<String, String> attributes) {
		StringBuilder result = new StringBuilder();
		attributes.forEach((k,v) -> result.append(String.format(" %s=\"%s\"", k, v)));
		return result.toString();
	}
}
// END
