package exercise;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class PairedTag extends Tag {

    private String body;
    private List<Tag> children;

	public PairedTag(final String name, final Map<String, String> attributes, final String body, final List<Tag> children) {
		super(name, attributes);
		this.body = body;
		this.children = children;
	}

	public String toString() {
		StringBuilder result = new StringBuilder(tag);
		children.forEach(tg -> result.append(tg));
		result.append(body).append(String.format("</%s>", name));
		return result.toString();
	}
}
// END
