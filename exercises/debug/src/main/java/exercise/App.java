package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a,int b, int c){
String result="";

        if (((a+b)>c)&&((a+c)>b)&&((b+c)>a)&&(a>0)&&(b>0)&&(c>0)) {
            if ((a==b)&&(a==c)) {
                result ="Равносторонний";
            } else if ((a==b)||(a==c)||(b==c)) {
                result ="Равнобедренный";
            } else {
                result ="Разносторонний";
            }

        } else {
            result = "Треугольник не существует";
        }

        return result;
    }
    // END
}
