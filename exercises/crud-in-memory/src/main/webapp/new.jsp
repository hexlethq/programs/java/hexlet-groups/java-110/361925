<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add new user</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
            crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <a href="/users">Все пользователи</a>
            <!-- BEGIN -->
            <p>Создание нового пользователя.</p>
            <%@ page import ="java.util.Map" %>
            <% 
                Map<String, String> user = (Map<String, String>) request.getAttribute("user");
                Integer status = (Integer) request.getAttribute("status") ; 
            %>
            <c:if test="${status == 422}" >
                <p style="color:red">Поля "First name" и "Last name" должны быть заполнены</p><br/>
            </c:if>
                <form action="/users/new" method="post">
                    First name: <input type="text" name="firstName" value="<%= user.get("firstName")%>" ><br/><br/>
                    Last name: <input type="text" name="lastName" value="<%= user.get("lastName")%>" ><br/><br/>
                    E-mail: <input type="email" name="email" value="<%= user.get("email")%>" ><br/><br/>
                    <button type="submit" class="btn btn-success">Создать пользователя</button>
                </form>
            
            <!-- END -->
        </div>
    </body>
</html>
