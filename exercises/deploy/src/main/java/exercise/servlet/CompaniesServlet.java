package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;
import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        List<String> companies = getCompanies();
        String query = request.getQueryString();
        String value = request.getParameter("search");
        PrintWriter out = response.getWriter();
        if (query == null || value == null) {
          companies.forEach(companyName -> out.println(companyName));
        } else if (value != null) {
          List<String> result = companies.stream()
          .filter(companyName -> companyName.contains(value))
          .collect(Collectors.toList());
          if (!result.isEmpty()) {
            result.forEach(companyName -> out.println(companyName));
          } else {
            out.println("Companies not found");
          }
        }
        // END
    }
}
