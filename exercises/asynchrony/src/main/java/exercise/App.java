package exercise;

import java.util.concurrent.CompletableFuture;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.*;
import java.util.stream.Collectors;
import java.lang.Exception;
import java.util.Objects;

class App {

    // BEGIN
    private static Path pathToApp = Path.of(".").toAbsolutePath().normalize();

    public static CompletableFuture<String> unionFiles(
            String file1,
            String file2,
            String destination) {

        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            try {
                return Files.readString(pathToApp.resolve(Path.of(file1)));
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        });
        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            try {
                return Files.readString(pathToApp.resolve(Path.of(file2)));
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        });
        CompletableFuture<String> result = future1.thenCombine(future2, (future1Result, future2Result) -> {
            String newFileContent = future1Result + future2Result;
            try {
                Files.writeString(pathToApp.resolve(Path.of(destination)),
                    newFileContent,
                    StandardOpenOption.CREATE);
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
            return "Success!";
        }).exceptionally(ex -> {
            System.out.println(ex.getMessage());
            return null;
        });
        return result;
    }

    public static CompletableFuture<Long> getDirectorySize(String pathToDir) {

        return CompletableFuture.supplyAsync(() -> {
            Long size;
            try {
                size = Files.walk(pathToApp.resolve(Path.of(pathToDir)), 1)
                    .filter(Files::isRegularFile)
                    .mapToLong(file -> {
                        try {
                            return Files.size(file);
                        } catch (IOException ioe) {
                            throw new IllegalStateException(ioe);
                        }
                    })
                    .sum();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            };
            return size;
        }).exceptionally(ex -> {
            System.out.println(ex.getMessage());
            return null;
        });
      
    }

    // END

    public static void main(String[] args) throws Exception {
        // BEGIN
        String resultContent = App.unionFiles("src/main/resources/file1.txt",
                "src/main/resources/file2.txt",
                "src/main/resources/result.txt").get();
        System.out.println(resultContent);

        Long dirSize = App.getDirectorySize("src/main/resources/").get();
        System.out.println(dirSize);
        // END
    }
}

