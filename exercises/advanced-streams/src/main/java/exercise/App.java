package exercise;

import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String getForwardedVariables(String content) {
       return
                content.lines()
                        .filter(line -> line.startsWith("environment"))
                        .filter(line -> line.contains("X_FORWARDED_"))
                        .map(line -> line.substring(line.indexOf("X_FORWARDED_")))
                        .flatMap(lines -> Stream.of(lines.split(","))
                                .filter(line -> line.trim().startsWith("X_FORWARDED_"))
                                .map(line -> line.replaceAll("X_FORWARDED_", ""))
                                .map(line -> line.replaceAll("\"", ""))
                        )
                        .collect(Collectors.joining(","));
    }
}
//END
